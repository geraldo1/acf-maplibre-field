(function($){
	
	
	/**
	*  initialize_field
	*
	*  This function will initialize the $field.
	*
	*  @date	30/11/17
	*  @since	5.6.5
	*
	*  @param	n/a
	*  @return	n/a
	*/

	var marker;
	
	function initialize_field( $field ) {

		// !!!lat & lng are inverted!!!
		
		// Get var from this file
        const field_key = 'acf-' + $field[0].dataset.key,
        	  fieldElt = $('#' + field_key).get(0),
        	  zoom = fieldElt.getAttribute('data-zoom'),
        	  center_lat = fieldElt.getAttribute('data-center-lat'),
        	  center_lng = fieldElt.getAttribute('data-center-lng');

        var lat = $('.maplibre-field-input-lat input').val(),
            lng = $('.maplibre-field-input-lng input').val(),
            coord = Array(parseFloat(lat), parseFloat(lng)),
        	center,
        	poligon = $('.maplibre-field-input-poligon textarea').val();

        if (isNumber(coord[0]) && isNumber(coord[1])) {
        	// set center and marker to saved position
        	center = coord;
        }
        else {
        	// set center to default center if not set and marker outside
        	center = [center_lat, center_lng];
        	coord = [0,0];
        }

        map = new maplibregl.Map({
	        container: 'backendmap',
		    style: '/tracesmap/20240408_traces_r03.json',
	        center: center,
	        zoom: zoom,
        	maxZoom: 20
	    });

        const icon = document.createElement('div');
        icon.className = 'marker';
        icon.style.backgroundImage =
            `url(https://tracesmap.org/wp-content/plugins/acf-map-field/assets/images/marker.png)`;
        icon.style.width = `29px`;
        icon.style.height = `39px`;

	    marker = new maplibregl.Marker({element: icon})
        .setLngLat(coord)
        .addTo(map);

        map.on('load', () => {

        	try {
				poligon = JSON.parse(poligon);

		        map.addSource('polygon', {
		            'type': 'geojson',
		            'data': {
		                'type': 'Feature',
		                'geometry': poligon
		            }
		        });
		        map.addLayer({
		            'id': 'polygon-line',
		            'type': 'line',
		            'source': 'polygon',
		            'layout': {},
		            'paint': {
		                'line-color': '#00f',
		                'line-width': 2
		            }
		        });
		        map.addLayer({
		            'id': 'polygon-fill',
		            'type': 'fill',
		            'source': 'polygon',
		            'layout': {},
		            'paint': {
		                'fill-color': '#00f',
		                'fill-opacity': 0.1
		            }
		        });

	        } catch (e) {}
	    });

        map.on('click', (evt) => {
            // set inputs to clicked coordinate
            $('.maplibre-field-input-lng input').val(evt.lngLat.lng);
            $('.maplibre-field-input-lat input').val(evt.lngLat.lat);

            setMarkerPos();
        });

        $('.maplibre-field-input-lng input').on('input', function(e) {
        	setMarkerPos();
        });

        $('.maplibre-field-input-lat input').on('input', function(e) {
        	setMarkerPos();
        });
	}

	function setMarkerPos() {
        let lng = $('.maplibre-field-input-lng input').val(),
            lat = $('.maplibre-field-input-lat input').val();

        console.log([lng,lat]);

        marker
        .setLngLat([lng,lat])
        .addTo(map);

    	map.flyTo({
            center: [lng, lat]
        });
	}
	
	
	if( typeof acf.add_action !== 'undefined' ) {
	
		/*
		*  ready & append (ACF5)
		*
		*  These two events are called when a field element is ready for initizliation.
		*  - ready: on page load similar to $(document).ready()
		*  - append: on new DOM elements appended via repeater field or other AJAX calls
		*
		*  @param	n/a
		*  @return	n/a
		*/
		
		acf.add_action('ready_field/type=maplibre_field', initialize_field);
		acf.add_action('append_field/type=maplibre_field', initialize_field);
		
		
	} else {
		
		/*
		*  acf/setup_fields (ACF4)
		*
		*  These single event is called when a field element is ready for initizliation.
		*
		*  @param	event		an event object. This can be ignored
		*  @param	element		An element which contains the new HTML
		*  @return	n/a
		*/
		
		$(document).on('acf/setup_fields', function(e, postbox){
			
			// find all relevant fields
			$(postbox).find('.field[data-field_type="maplibre_field"]').each(function(){
				
				// initialize
				initialize_field( $(this) );
				
			});
		
		});
	
	}

	var isNumber = function isNumber(value) {
		return typeof value === 'number' && isFinite(value);
	}

})(jQuery);
