<?php

/*
Plugin Name: ACF Maplibre Field
Plugin URI: https://gitlab.com/geraldo1/acf-maplibre-field
Description: This plugin offers a ACF map field which uses external latitude and longitude fields to show and select a location on a Maplibre GL JS map in Wordpress backend and show it in Wordpress frontend.
Version: 1.0.0
Author: Gerald Kogler
Author URI: https://go.yuri.at/
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('acf_plugin_maplibre_field') ) :

class acf_plugin_maplibre_field {
	
	// vars
	var $settings;
	
	
	/*
	*  __construct
	*
	*  This function will setup the class functionality
	*
	*  @type	function
	*  @date	17/02/2016
	*  @since	1.0.0
	*
	*  @param	void
	*  @return	void
	*/
	
	function __construct() {
		
		// settings
		// - these will be passed into the field class.
		$this->settings = array(
			'version'	=> '1.0.0',
			'url'		=> plugin_dir_url( __FILE__ ),
			'path'		=> plugin_dir_path( __FILE__ )
		);
		
		
		// include field
		add_action('acf/include_field_types', 	array($this, 'include_field')); // v5
	}
	
	
	/*
	*  include_field
	*
	*  This function will include the field type class
	*
	*  @type	function
	*  @date	17/02/2016
	*  @since	1.0.0
	*
	*  @param	$version (int) major ACF version. Defaults to false
	*  @return	void
	*/
	
	function include_field( $version = false ) {
		
		// support empty $version
		if( !$version ) $version = 5;
		
		
		// load textdomain
		load_plugin_textdomain( 'maplibre-field', false, plugin_basename( dirname( __FILE__ ) ) . '/lang' ); 
		
		
		// include
		include_once('fields/class-acf-maplibre-field-v' . $version . '.php');
	}
	
}


// initialize
new acf_plugin_maplibre_field();


// class_exists check
endif;
	
?>