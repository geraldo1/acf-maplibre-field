=== ACF Maplibre Field ===
Contributors: Gerald Kogler
Tags: acf, maplibre, map, location, latitude, longitude, poi
Requires at least: 3.6.0
Tested up to: 6.5
Stable tag: trunk
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

This plugin offers a ACF map field which uses external latitude and longitude fields to show and select a location on a Maplibre map in Wordpress backend and show it in Wordpress frontend. This plugin is heavily inspired by https://gitlab.com/geraldo1/acf-map-field, which is the Openlayers version with the same funcionality.

== Description ==

Using this field type adds a Maplibre map to your post form on backend. Clicking on the map sets the marker position and shows it in the latitude and longitude text fields. Editing latitude and longitude text inputs moves the marker position.

Note: To make this field work you have to define a ACF Group called `coordinates` with subfields called `latitude` and `longitude`.

Latitude and longitude are saved as float values like that - but NOT inside this field but as ACF number fields.

Initial map center and zoom value can be set in the field properties.

= Compatibility =

This ACF field type is compatible with ACF 5

== Installation ==

1. Copy the `acf-maplibre-field` folder into your `wp-content/plugins` folder
2. Activate the Maplibre Field plugin via the plugins admin page
3. Create a new field via ACF and select the Maplibre Field type
4. Read the description above for usage instructions

== Changelog ==

= 1.0.0 =
* Initial Release.